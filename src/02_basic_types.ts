/**
 * Implicitly typed by TypeScript
 */
const paragraph = 'Text';

/**
 * Explicitly typed by me
 */
const numberAsString: string = '5';

/**
 * Typing an object
 */
type ObjectType = {
    numProp: number;
    stringProp: string;
    optionalProp?: string; // optional

    functionProp?: () => void;
};

const obj: ObjectType = {
    numProp: 5,
    stringProp: 'str',
    optionalProp: undefined,
};

const implicitlyTyped = {
    numProp: 2,
    stringProp: '',
};

/**
 * Typing a function
 */
const add = (left: ObjectType, right: ObjectType): number => {
    return left.numProp + right.numProp;
};

/**
 * Function accepts every object that fulfills type/interface
 * Structural Typing (Duck Typing)
 */
const result = add(obj, implicitlyTyped);

class Duck {
    public talk = () => 'Quack';
}

const funWithDucks = (obj: Duck) => obj;

funWithDucks({ talk: () => 'Wuff' });

/**
 * User-defined type guard
 */
const ensureType = (objectToCheck: unknown): objectToCheck is ObjectType =>
    typeof (objectToCheck as ObjectType).numProp === 'number' &&
    typeof (objectToCheck as ObjectType).stringProp === 'string';

const randomObject: unknown = {};

if (ensureType(randomObject)) {
    console.log(randomObject.numProp);
}

/**
 * Tuple Type
 */
type ArrayWithDifferentEntries = Array<string | number | boolean>;
type ArrayWithSpecificStructure = [string, number, boolean];

const arr1: ArrayWithDifferentEntries = ['str', 1, true, 'and more', false];
const arr2: ArrayWithSpecificStructure = ['str', 1, true];
