let vAny: any = 10;
let vUnknown: unknown = 10;

let s1: string = vAny;
let s2: string = vUnknown;

vAny.method();
vUnknown.method();

if (hasMethod(vUnknown)) {
    vUnknown.method();
}

if (typeof vUnknown === 'string') {
    console.log(vUnknown.trim());
}

interface WithMethod {
    method: () => void;
}

const hasMethod = (obj: unknown): obj is WithMethod =>
    !!(obj as WithMethod).method;

type Origin = {
    a: string;
    b: string;
    _c: string;
    _d: string;
};

type FilterNotStartingWith<
    Set,
    Needle extends string
> = Set extends `${Needle}${infer _X}` ? never : Set;

type FilteredKeys = FilterNotStartingWith<keyof Origin, '_'>;
type NewOrigin = Pick<Origin, FilteredKeys>;
