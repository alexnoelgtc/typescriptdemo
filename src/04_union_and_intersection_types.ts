/**
 * Some variable could have different types
 */
let numberOrText: number | string = 5;
numberOrText = 'foo';

type Config = {
    params: string | Array<string>;
    method: 'GET' | 'PUT' | 'POST';
    status: ExtractedType;
};

type ExtractedType = 'loading' | 'success' | 'error' | 'idle';

const config: Config = {
    params: ['schnell', 'gut'],
    status: 'idle',
    method: 'GET',
};

/**
 * Union type as parameter
 */
const setStatus = (configuration: Config, newStatus: ExtractedType) => {
    configuration.status = newStatus;
};

setStatus(config, 'loading');

/**
 * TypeScript is pretty smart with union types
 */
function welcomePeople(x: string[] | string) {
    if (Array.isArray(x)) {
        // Here: 'x' is 'string[]'
        console.log('Hello, ' + x.join(' and '));
    } else {
        // Here: 'x' is 'string'
        console.log('Welcome lone traveler ' + x);
    }
}

/**
 * Functions can be overloaded
 */
function combine(a: string, b: string): string;
function combine(a: number, b: number): number;
function combine(a: any, b: any): any {
    return a + b;
}

const str = combine('Hello ', 'World');
const num = combine(10, 20);

/**
 * Intersection Types
 */
interface ErrorHandling {
    success: boolean;
    error?: { message: string };
}

interface ArtworksData {
    artworks: Array<{ title: string }>;
}

interface ArtistsData {
    artists: Array<{ name: string }>;
}

type ArtworksResponse = ArtworksData & ErrorHandling;
type ArtistsResponse = ArtistsData & ErrorHandling;

const artworksResponse: ArtworksResponse = {
    artworks: [{ title: 'myTitle' }],
    success: false,
    error: new Error('I have a message'),
};
