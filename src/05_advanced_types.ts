/**
 * Index Signatures
 */
type KeyValue = {
    [index: string]: string;
};

const keyValue: KeyValue = {
    anyProperty: 'with string value',
    seriouslyAnyProperty: 'just got to have a string value',
};

type StringArray = {
    [index: number]: string;
};

const arrayLike: StringArray = ['', ''];

/**
 * Extending Types
 */
type ChildComponentProps = {
    color: string;
};

type WrapperProps = {
    size: number;
} & ChildComponentProps;

/**
 * keyof produces an union of its keys
 */
type Point = { x: number; y: number };
type P = keyof Point;
const keyofPoint: P = 'x';

/**
 * typeof operator can be used in a type context to refer to the type of a variable or property
 */
const zipCodes = {
    '1067': {
        bundesland: 'Sachsen',
        kreis: 'Dresden',
    },
    '85084': {
        bundesland: 'Bayern',
        kreis: 'Pfaffenhofen an der Ilm',
    },
};
type ZipCode = keyof typeof zipCodes;
const zipCode: ZipCode = '85084';

/**
 * ReturnType
 */
function returnSomething() {
    return { x: 10, y: 3 };
}
type TypeOfF = ReturnType<typeof returnSomething>;

/**
 * Mapped Types
 */
type LoadingState = 'loading' | 'success' | 'error' | 'idle';
type LoadingStateToText = {
    [key in LoadingState]: string;
};

const loadingStateToText: LoadingStateToText = {
    error: 'Hier war etwas falsch',
    idle: 'Ich mach gar nichts',
    loading: 'Gleich sind wir soweit',
    success: 'We did it!',
};
