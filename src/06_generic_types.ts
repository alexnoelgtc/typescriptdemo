/**
 * Generic type in a function
 */
const removeValueFromArray = <T>(
    arr: Array<T>,
    valueToBeRemoved: T
): Array<T> => arr.filter((value) => value !== valueToBeRemoved);

const arr = ['foo', 'bar', 'baz'];
const arrayWithRemovedValue = removeValueFromArray(arr, 'foo');

/**
 * Generic type in a object type declaration
 */
type Table<Row> = {
    items: Array<Row>;
    onRowClick: (row: Row) => void;
};

type WithId = {
    id: number;
};

const table: Table<WithId> = {
    items: [{ id: 1 }],
    onRowClick: (row) => console.log(row.id),
};

/**
 * With restriction
 */
const getProperty = <Obj, T extends keyof Obj>(obj: Obj, key: T) => obj[key];

const object = { a: 1, b: '2' };

getProperty(object, 'a');

/**
 * With default value
 */
const makeList = <ObjectType = string>(): Array<ObjectType> => [];

const numberList = makeList<WithId>();
const stringList = makeList();
const booleanList: Array<boolean> = makeList();
