/**
 * Utility Types provide common type transformations
 */

/**
 * Partial constructs a type with all properties of Type set to optional.
 */
type Todo = {
    title: string;
    description: string;
    author: string;
    createdAt: string;
    updatedAt: string;
};

const updateTodo = (todo: Todo, fieldsToUpdate: Partial<Todo>) => ({
    ...todo,
    ...fieldsToUpdate,
});

const todo1: Todo = {
    title: 'organize desk',
    description: 'clear clutter',
    author: 'Me',
    createdAt: '2020-02-02',
    updatedAt: '2021-01-01',
};

const todo2 = updateTodo(todo1, {
    description: 'throw out trash',
});

/**
 * Pick constructs a type by picking the set of properties Keys
 */
type TodoContent = Pick<Todo, 'title' | 'description'>;

const todoContent: TodoContent = {
    title: 'title',
    description: 'desc',
};

/**
 * Omit constructs a type by picking all properties from Type and then removing Keys
 */
type TodoWithoutTimeData = Omit<Todo, 'updatedAt' | 'createdAt'>;

const reducedTodo: TodoWithoutTimeData = {
    title: 'title',
    description: 'desc',
    author: 'You',
};

// And there are many more
